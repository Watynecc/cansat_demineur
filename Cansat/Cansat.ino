/*
  Mission Principale CanSat LXM
  version 2023 fait par Watynecc ;D
  PML

*/

#define buzzer 3
#include <SparkFunBME280.h>
#include <Wire.h>
#include <SoftwareSerial.h>
SoftwareSerial apc(7,8);  // rx/tx
BME280 capteur; //I2C addresse0x76 (pas de signal sur SD0)

int i;
int frequence[] = {262, 294, 330, 349, 370, 392, 440, 494};      // tableau de fréquence des notes

int larg = 5;
long Toplarg = 0;
bool drapeau1 = false;
long Tempslarg = 0;
long Enregistrement = 0;
long buz = 200000;
int ledrouge = 2;



void setup() {

  // Définition des entrées & sorties
  pinMode(ledrouge, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(larg, INPUT_PULLUP);

  // ouverture de la communication série
  Serial.begin(19200);
  apc.begin(19200);
  Serial.println("Utilisation du capteur BMP280 en I2C");

  Wire.begin();

  capteur.setI2CAddress(0x76); //Utilisation de l'adresse 0x76 il faut relier SD0 sur GND

  if (capteur.beginI2C() == false) Serial.println("Connection du capteur : echec");


}

void loop() {
  // la led rouge reste allummée si aucune action
    apc.println("Test");
    digitalWrite(ledrouge, HIGH);

    // detection largage

    if (digitalRead(larg) == 1 & drapeau1 == false) {
        Toplarg = millis();
        Tempslarg = Toplarg;
        drapeau1 = true;
        Serial.print("Largage !!!");
    }

    // Calcul du temps depuis le largage

    if (drapeau1 == true) Tempslarg = millis() - Toplarg;

    if (Tempslarg > (Enregistrement + 999)) {
        Enregistrement = Tempslarg;

        // Mesure de la pression

        Serial.print(" Pression: ");
        Serial.print(capteur.readFloatPressure(), 0);
        Serial.print(" kPa --- ");

        // mesure de la température

        Serial.print(" Température : ");
        Serial.print(capteur.readTempC(), 2);
        Serial.println(" °C ");

        Serial.println();

        // clignotement led

        if (Tempslarg < buz) {
            digitalWrite(ledrouge, LOW);
            delay(400);
        }
    }

    if (Tempslarg > buz) {

        for (int i = 0; i < 8; i++) {
            tone(buzzer, frequence[i], 225);         // la note est jouée pendant 300 ms
            delay(125);                              // attente de 100 ms entre chaque note
        }
    }
}
